namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FAMILLE_REPAS
    {
        [Key]
        [Column(Order = 0)]
        public int ID_FAMILLE_REPAS { get; set; }

        [Column(Order = 1)]
        [StringLength(255)]
        public string LIBELLE_FAMILLE_REPAS { get; set; }
    }
}
