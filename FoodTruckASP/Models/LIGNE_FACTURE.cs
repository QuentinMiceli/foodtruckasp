namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LIGNE_FACTURE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_LIGNE_FACTURE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_FACTURE { get; set; }

        [Column(Order = 2)]
        public double QUANTITE { get; set; }

        [Column(Order = 3, TypeName = "numeric")]
        public decimal PRIX_UNITAIRE { get; set; }
    }
}
