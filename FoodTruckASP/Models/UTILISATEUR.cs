namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UTILISATEUR")]
    public partial class UTILISATEUR
    {
        [Key]
        [Column(Order = 0)]
        public int ID_UTILISATEUR { get; set; }

        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_GENRE { get; set; }


        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_PROFIL { get; set; }

        public int? ID_SOCIETE { get; set; }


        [Column(Order = 3)]
        [StringLength(30)]
        public string NOM { get; set; }


        [Column(Order = 4)]
        [StringLength(50)]
        public string PRENOM { get; set; }


        [Column(Order = 5)]
        [StringLength(255)]
        public string MOT_DE_PASSE { get; set; }


        [Column(Order = 6)]
        [StringLength(100)]
        public string EMAIL { get; set; }


        [Column(Order = 7, TypeName = "date")]
        public DateTime DATE_DE_NAISSANCE { get; set; }

        [StringLength(20)]
        public string TEL { get; set; }
    }
}
