namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SOCIETE")]
    public partial class SOCIETE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_SOCIETE { get; set; }

        [Column(Order = 1)]
        [StringLength(50)]
        public string LIBELLE_SOCIETE { get; set; }


        [Column(Order = 2)]
        [StringLength(10)]
        public string NUMERO_RUE { get; set; }


        [Column(Order = 3)]
        [StringLength(30)]
        public string RUE { get; set; }


        [Column(Order = 4)]
        [StringLength(6)]
        public string CODE_POSTAL { get; set; }


        [Column(Order = 5)]
        [StringLength(50)]
        public string VILLE { get; set; }


        [Column(Order = 6)]
        [StringLength(30)]
        public string PAYS { get; set; }
    }
}
