namespace FoodTruckASP.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<ACTUALITES> ACTUALITES { get; set; }
        public virtual DbSet<ADRESSE> ADRESSE { get; set; }
        public virtual DbSet<ALLERGENE> ALLERGENE { get; set; }
        public virtual DbSet<COMMANDE> COMMANDE { get; set; }
        public virtual DbSet<COMMANDE_STATUT> COMMANDE_STATUT { get; set; }
        public virtual DbSet<FACTURE> FACTURE { get; set; }
        public virtual DbSet<FAMILLE_REPAS> FAMILLE_REPAS { get; set; }
        public virtual DbSet<GENRE> GENRE { get; set; }
        public virtual DbSet<LIGNE_COMMANDE> LIGNE_COMMANDE { get; set; }
        public virtual DbSet<LIGNE_FACTURE> LIGNE_FACTURE { get; set; }
        public virtual DbSet<NOTE> NOTE { get; set; }
        public virtual DbSet<PARAMETRAGE> PARAMETRAGE { get; set; }
        public virtual DbSet<PRODUIT> PRODUIT { get; set; }
        public virtual DbSet<PRODUIT_ALLERGENE> PRODUIT_ALLERGENE { get; set; }
        public virtual DbSet<PROFIL> PROFIL { get; set; }
        public virtual DbSet<SOCIETE> SOCIETE { get; set; }
        public virtual DbSet<TYPE_FAMILLE_REPAS> TYPE_FAMILLE_REPAS { get; set; }
        public virtual DbSet<TYPE_REPAS> TYPE_REPAS { get; set; }
        public virtual DbSet<UTILISATEUR> UTILISATEUR { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACTUALITES>()
                .Property(e => e.TITRE)
                .IsUnicode(false);

            modelBuilder.Entity<ACTUALITES>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ACTUALITES>()
                .Property(e => e.URL_IMAGE)
                .IsUnicode(false);

            modelBuilder.Entity<ACTUALITES>()
                .Property(e => e.LINK)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.LIBELLE_ADRESSE)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.NUMERO_RUE)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.RUE)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.CODE_POSTAL)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.VILLE)
                .IsUnicode(false);

            modelBuilder.Entity<ADRESSE>()
                .Property(e => e.PAYS)
                .IsUnicode(false);

            modelBuilder.Entity<ALLERGENE>()
                .Property(e => e.LIBELLE_ALLERGENE)
                .IsUnicode(false);

            modelBuilder.Entity<ALLERGENE>()
                .Property(e => e.CODE_ALLERGENE)
                .IsUnicode(false);

            modelBuilder.Entity<COMMANDE_STATUT>()
                .Property(e => e.LIBELLE_COMMANDE_STATUT)
                .IsUnicode(false);

            modelBuilder.Entity<FAMILLE_REPAS>()
                .Property(e => e.LIBELLE_FAMILLE_REPAS)
                .IsUnicode(false);

            modelBuilder.Entity<GENRE>()
                .Property(e => e.LIBELLE_GENRE)
                .IsUnicode(false);

            modelBuilder.Entity<NOTE>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<PARAMETRAGE>()
                .Property(e => e.PLAGE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUIT>()
                .Property(e => e.LIBELLE_PRODUIT)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUIT>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUIT>()
                .Property(e => e.URL_IMAGE)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUIT>()
                .Property(e => e.LMMJVSD)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUIT>()
                .Property(e => e.UNITE)
                .IsUnicode(false);

            modelBuilder.Entity<PROFIL>()
                .Property(e => e.LIBELLE_PROFIL)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.LIBELLE_SOCIETE)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.NUMERO_RUE)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.RUE)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.CODE_POSTAL)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.VILLE)
                .IsUnicode(false);

            modelBuilder.Entity<SOCIETE>()
                .Property(e => e.PAYS)
                .IsUnicode(false);

            modelBuilder.Entity<TYPE_REPAS>()
                .Property(e => e.LIBELLE_REPAS)
                .IsUnicode(false);

            modelBuilder.Entity<UTILISATEUR>()
                .Property(e => e.NOM)
                .IsUnicode(false);

            modelBuilder.Entity<UTILISATEUR>()
                .Property(e => e.PRENOM)
                .IsUnicode(false);

            modelBuilder.Entity<UTILISATEUR>()
                .Property(e => e.MOT_DE_PASSE)
                .IsUnicode(false);

            modelBuilder.Entity<UTILISATEUR>()
                .Property(e => e.EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<UTILISATEUR>()
                .Property(e => e.TEL)
                .IsUnicode(false);
        }
    }
}
