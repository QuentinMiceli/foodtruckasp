namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ALLERGENE")]
    public partial class ALLERGENE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_ALLERGENE { get; set; }

        [Column(Order = 1)]
        [StringLength(255)]
        public string LIBELLE_ALLERGENE { get; set; }

        [StringLength(5)]
        public string CODE_ALLERGENE { get; set; }
    }
}
