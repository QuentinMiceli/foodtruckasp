namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACTUALITES
    {
        [Key]
        [Column(Order = 0)]
        public int ID_ACTUALITE { get; set; }

        [Column(Order = 1)]
        [StringLength(255)]
        public string TITRE { get; set; }

        [Column(Order = 2, TypeName = "text")]
        public string DESCRIPTION { get; set; }

        [Column(Order = 3)]
        [StringLength(255)]
        public string URL_IMAGE { get; set; }

        [Column(Order = 4)]
        public DateTime DATE_DEBUT { get; set; }

        [Column(Order = 5)]
        public DateTime DATE_FIN { get; set; }

        [Column(Order = 6)]
        [StringLength(255)]
        public string LINK { get; set; }

        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_UTILISATEUR { get; set; }

        [Column(Order = 8)]
        public bool ACTIF { get; set; }
    }
}
