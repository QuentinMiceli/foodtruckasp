namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("COMMANDE")]
    public partial class COMMANDE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_COMMANDE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_UTILISATEUR { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ADRESSE { get; set; }

        [Column(Order = 3)]
        public DateTime DATE_COMMANDE { get; set; }

        [Column(Order = 4, TypeName = "numeric")]
        public decimal TOTAL_COMMANDE { get; set; }

        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_COMMANDE_STATUS { get; set; }
    }
}
