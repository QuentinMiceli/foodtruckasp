﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FoodTruckASP.Models
{
    public class ACTUALITESController : Controller
    {
        private Context db = new Context();

        // GET: ACTUALITES
        public ActionResult Index()
        {
            return View(db.ACTUALITES.ToList());
        }

        // GET: ACTUALITES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACTUALITES aCTUALITES = db.ACTUALITES.Find(id);
            if (aCTUALITES == null)
            {
                return HttpNotFound();
            }
            return View(aCTUALITES);
        }

        // GET: ACTUALITES/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ACTUALITES/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_ACTUALITE,TITRE,DESCRIPTION,URL_IMAGE,DATE_DEBUT,DATE_FIN,LINK,ID_UTILISATEUR,ACTIF")] ACTUALITES aCTUALITES)
        {
            if (ModelState.IsValid)
            {
                db.ACTUALITES.Add(aCTUALITES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aCTUALITES);
        }

        // GET: ACTUALITES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACTUALITES aCTUALITES = db.ACTUALITES.Find(id);
            if (aCTUALITES == null)
            {
                return HttpNotFound();
            }
            return View(aCTUALITES);
        }

        // POST: ACTUALITES/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_ACTUALITE,TITRE,DESCRIPTION,URL_IMAGE,DATE_DEBUT,DATE_FIN,LINK,ID_UTILISATEUR,ACTIF")] ACTUALITES aCTUALITES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aCTUALITES).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aCTUALITES);
        }

        // GET: ACTUALITES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACTUALITES aCTUALITES = db.ACTUALITES.Find(id);
            if (aCTUALITES == null)
            {
                return HttpNotFound();
            }
            return View(aCTUALITES);
        }

        // POST: ACTUALITES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ACTUALITES aCTUALITES = db.ACTUALITES.Find(id);
            db.ACTUALITES.Remove(aCTUALITES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
