namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FACTURE")]
    public partial class FACTURE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_FACTURE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_COMMANDE { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ADRESSE { get; set; }

        [Column(Order = 3, TypeName = "numeric")]
        public decimal TOTAL_FACTURE { get; set; }

        [Column(Order = 4)]
        public bool FACTUREE { get; set; }
    }
}
