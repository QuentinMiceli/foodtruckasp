namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ADRESSE")]
    public partial class ADRESSE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ADRESSE { get; set; }

        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_UTILISATEUR { get; set; }

        [Column(Order = 2)]
        [StringLength(50)]
        public string LIBELLE_ADRESSE { get; set; }

        [Column(Order = 3)]
        public bool ACTIF { get; set; }

        [Column(Order = 4)]
        [StringLength(10)]
        public string NUMERO_RUE { get; set; }

        [Column(Order = 5)]
        [StringLength(30)]
        public string RUE { get; set; }

        [Column(Order = 6)]
        [StringLength(6)]
        public string CODE_POSTAL { get; set; }

        [Column(Order = 7)]
        [StringLength(50)]
        public string VILLE { get; set; }

        [Column(Order = 8)]
        [StringLength(30)]
        public string PAYS { get; set; }
    }
}
