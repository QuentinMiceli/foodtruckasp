namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRODUIT")]
    public partial class PRODUIT
    {
        [Key]
        [Column(Order = 0)]
        public int ID_PRODUIT { get; set; }


        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_FAMILLE_REPAS { get; set; }


        [Column(Order = 2)]
        [StringLength(30)]
        public string LIBELLE_PRODUIT { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }


        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NOMBRE_DE_VENTE { get; set; }


        [Column(Order = 4, TypeName = "numeric")]
        public decimal PRIX { get; set; }


        [Column(Order = 5)]
        [StringLength(255)]
        public string URL_IMAGE { get; set; }

        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int STOCK { get; set; }

        [StringLength(7)]
        public string LMMJVSD { get; set; }

        public double? MOYENNE_NOTE { get; set; }

        [StringLength(10)]
        public string UNITE { get; set; }
    }
}
