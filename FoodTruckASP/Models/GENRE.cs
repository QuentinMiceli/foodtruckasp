namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GENRE")]
    public partial class GENRE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_GENRE { get; set; }

        [Column(Order = 1)]
        [StringLength(20)]
        public string LIBELLE_GENRE { get; set; }
    }
}
