namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NOTE")]
    public partial class NOTE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_NOTE { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_PRODUIT { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_COMMANDE { get; set; }

        [Column("NOTE", Order = 3)]
        public double NOTE1 { get; set; }

        [Column(Order = 4)]
        public DateTime DATE_NOTE { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        [Column(Order = 5)]
        public bool ACTIF { get; set; }
    }
}
