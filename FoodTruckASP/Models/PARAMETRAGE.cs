namespace FoodTruckASP.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PARAMETRAGE")]
    public partial class PARAMETRAGE
    {
        [Key]
        [Column(Order = 0)]
        public int ID_PARAMETRAGE { get; set; }

        [Column(Order = 1)]
        [StringLength(255)]
        public string PLAGE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PRIX { get; set; }
    }
}
