﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodTruckASP.Models
{
    public class HomeViewModel
    {
        public List<PRODUIT> ListProduits { get; set; }
        public List<ACTUALITES> ListActualites { get; set; }

        public HomeViewModel()
        {
            ListProduits = new List<PRODUIT>();
            ListActualites = new List<ACTUALITES>();
        }
    }
}