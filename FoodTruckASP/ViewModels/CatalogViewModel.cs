﻿using FoodTruckASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodTruckASP.ViewModels
{
    public class CatalogViewModel
    {
        public List<PRODUIT> ListProduits { get; set; }
        public List<TYPE_REPAS> ListTypeRepas { get; set; }
        public List<FAMILLE_REPAS> ListFamilleRepas { get; set; }
        public string searchProduct { get; set; }
        public int selectedFormTypeRepas { get; set; }
        public int selectedFormFamilleRepas { get; set; }

        public CatalogViewModel()
        {
            ListProduits = new List<PRODUIT>();
            ListTypeRepas = new List<TYPE_REPAS>();
            ListFamilleRepas = new List<FAMILLE_REPAS>();
            searchProduct = "";
            selectedFormTypeRepas = 0;
            selectedFormFamilleRepas = 0;
        }
    }
}