﻿$().ready(() => {
    $("#selectedTypeRepas").change(() => {
        $("#searchForm").submit();
    });
    $("#selectedFamilleRepas").change(() => {
        $("#searchForm").submit();
    });
    $("#searchProduct").change(() => {
        $("#searchForm").submit();
    });
    $("#clearBtn").click(() => {
        $("#selectedTypeRepas").val(0);
        $("#selectedFamilleRepas").val(0);
        $("#searchProduct").val("");
        $("#searchForm").submit();
    });
});