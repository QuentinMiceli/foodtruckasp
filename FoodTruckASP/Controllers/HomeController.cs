﻿using FoodTruckASP.Models;
using FoodTruckASP.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodTruckASP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HomeViewModel vm = new HomeViewModel();
            using (Context db = new Context())
            {
                vm.ListProduits = db.PRODUIT.OrderByDescending(x => x.NOMBRE_DE_VENTE).Take(3).ToList();
                vm.ListActualites = db.ACTUALITES.OrderByDescending(x => x.DATE_DEBUT).Take(3).ToList();
            }
            return View(vm);
        }
        public ActionResult Catalog(int currentPage = 0, int pageSize = ParametrageProject._PAGE_SIZE)
        {
            var vm = new CatalogViewModel();
            using (Context db = new Context())
            {
                vm.ListProduits = db.PRODUIT.OrderBy(x => x.LIBELLE_PRODUIT)
                                            .Skip(currentPage * pageSize)
                                            .Take(pageSize)
                                            .ToList();
                vm.ListTypeRepas = db.TYPE_REPAS.OrderBy(x => x.LIBELLE_REPAS).ToList();
                vm.ListFamilleRepas = db.FAMILLE_REPAS.ToList();
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult Catalog(int selectedTypeRepas, int selectedFamilleRepas, string searchProduct, int currentPage = 0, int pageSize = ParametrageProject._PAGE_SIZE)
        {
            var vm = new CatalogViewModel();
            vm.selectedFormFamilleRepas = selectedFamilleRepas;
            vm.selectedFormTypeRepas = selectedTypeRepas;

            using (Context db = new Context())
            {
                vm.ListProduits = db.PRODUIT.OrderBy(x => x.LIBELLE_PRODUIT)
                                            .Skip(currentPage * pageSize)
                                            .Take(pageSize)
                                            .ToList();
                vm.ListTypeRepas = db.TYPE_REPAS.OrderBy(x => x.LIBELLE_REPAS).ToList();
                vm.ListFamilleRepas = db.FAMILLE_REPAS.ToList();
            }
            return View(vm);
        }

        public ActionResult Admin()
        {
            return View();
        }
    }
}