﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodTruckASP.Models;

namespace FoodTruckASP.Controllers
{
    public class TYPE_REPASController : Controller
    {
        private Context db = new Context();

        // GET: TYPE_REPAS
        public ActionResult Index()
        {
            return View(db.TYPE_REPAS.ToList());
        }

        // GET: TYPE_REPAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_REPAS tYPE_REPAS = db.TYPE_REPAS.Find(id);
            if (tYPE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_REPAS);
        }

        // GET: TYPE_REPAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TYPE_REPAS/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_TYPE_REPAS,LIBELLE_REPAS")] TYPE_REPAS tYPE_REPAS)
        {
            if (ModelState.IsValid)
            {
                db.TYPE_REPAS.Add(tYPE_REPAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tYPE_REPAS);
        }

        // GET: TYPE_REPAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_REPAS tYPE_REPAS = db.TYPE_REPAS.Find(id);
            if (tYPE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_REPAS);
        }

        // POST: TYPE_REPAS/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_TYPE_REPAS,LIBELLE_REPAS")] TYPE_REPAS tYPE_REPAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tYPE_REPAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tYPE_REPAS);
        }

        // GET: TYPE_REPAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TYPE_REPAS tYPE_REPAS = db.TYPE_REPAS.Find(id);
            if (tYPE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(tYPE_REPAS);
        }

        // POST: TYPE_REPAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TYPE_REPAS tYPE_REPAS = db.TYPE_REPAS.Find(id);
            db.TYPE_REPAS.Remove(tYPE_REPAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
