﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodTruckASP.Models;

namespace FoodTruckASP.Controllers
{
    public class PRODUITController : Controller
    {
        private Context db = new Context();

        // GET: PRODUITs
        public ActionResult Index()
        {
            return View(db.PRODUIT.ToList());
        }

        // GET: PRODUITs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUIT pRODUIT = db.PRODUIT.Find(id);
            if (pRODUIT == null)
            {
                return HttpNotFound();
            }
            return View(pRODUIT);
        }

        // GET: PRODUITs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PRODUITs/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_PRODUIT,ID_FAMILLE_REPAS,LIBELLE_PRODUIT,DESCRIPTION,NOMBRE_DE_VENTE,PRIX,URL_IMAGE,STOCK,LMMJVSD,MOYENNE_NOTE,UNITE")] PRODUIT pRODUIT)
        {
            if (ModelState.IsValid)
            {
                db.PRODUIT.Add(pRODUIT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pRODUIT);
        }

        // GET: PRODUITs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUIT pRODUIT = db.PRODUIT.Find(id);
            if (pRODUIT == null)
            {
                return HttpNotFound();
            }
            return View(pRODUIT);
        }

        // POST: PRODUITs/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_PRODUIT,ID_FAMILLE_REPAS,LIBELLE_PRODUIT,DESCRIPTION,NOMBRE_DE_VENTE,PRIX,URL_IMAGE,STOCK,LMMJVSD,MOYENNE_NOTE,UNITE")] PRODUIT pRODUIT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pRODUIT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pRODUIT);
        }

        // GET: PRODUITs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUIT pRODUIT = db.PRODUIT.Find(id);
            if (pRODUIT == null)
            {
                return HttpNotFound();
            }
            return View(pRODUIT);
        }

        // POST: PRODUITs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PRODUIT pRODUIT = db.PRODUIT.Find(id);
            db.PRODUIT.Remove(pRODUIT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
