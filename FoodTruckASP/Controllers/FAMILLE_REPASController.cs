﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodTruckASP.Models;

namespace FoodTruckASP.Controllers
{
    public class FAMILLE_REPASController : Controller
    {
        private Context db = new Context();

        // GET: FAMILLE_REPAS
        public ActionResult Index()
        {
            return View(db.FAMILLE_REPAS.ToList());
        }

        // GET: FAMILLE_REPAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAMILLE_REPAS fAMILLE_REPAS = db.FAMILLE_REPAS.Find(id);
            if (fAMILLE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(fAMILLE_REPAS);
        }

        // GET: FAMILLE_REPAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FAMILLE_REPAS/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_FAMILLE_REPAS,LIBELLE_FAMILLE_REPAS")] FAMILLE_REPAS fAMILLE_REPAS)
        {
            if (ModelState.IsValid)
            {
                db.FAMILLE_REPAS.Add(fAMILLE_REPAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fAMILLE_REPAS);
        }

        // GET: FAMILLE_REPAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAMILLE_REPAS fAMILLE_REPAS = db.FAMILLE_REPAS.Find(id);
            if (fAMILLE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(fAMILLE_REPAS);
        }

        // POST: FAMILLE_REPAS/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_FAMILLE_REPAS,LIBELLE_FAMILLE_REPAS")] FAMILLE_REPAS fAMILLE_REPAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fAMILLE_REPAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fAMILLE_REPAS);
        }

        // GET: FAMILLE_REPAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAMILLE_REPAS fAMILLE_REPAS = db.FAMILLE_REPAS.Find(id);
            if (fAMILLE_REPAS == null)
            {
                return HttpNotFound();
            }
            return View(fAMILLE_REPAS);
        }

        // POST: FAMILLE_REPAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FAMILLE_REPAS fAMILLE_REPAS = db.FAMILLE_REPAS.Find(id);
            db.FAMILLE_REPAS.Remove(fAMILLE_REPAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
